# Mary Jardin Order Tracking App

Ordering app meant to be used for agent-based ordering network like Mary Jardin. I am not affiliated
to Mary Jardin. This app is just me trying to learn how to develop web apps in Rails. This app is
still a work in progress.

## Getting Started

### Prerequisites

Install [PostgreSQL](https://www.postgresql.org/) database to your machine.

### Installing

Clone this repository and run ```bundle install``` to install all the gems needed. Change the
```username``` and ```password``` for Postgres in ```config/secrets.yml``` file.

Run ```rails server``` and visit ```localhost:3000``` in your favorite browser.

## Running the tests

Execute ```rails test``` to run all of the test cases. **Note:** some of the test cases will fail.

## Contributing

## License

## Acknowledgments
